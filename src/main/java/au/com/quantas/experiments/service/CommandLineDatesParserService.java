package au.com.quantas.experiments.service;

import au.com.quantas.experiments.domain.DateRange;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public class CommandLineDatesParserService {

    static CommandLineDatesParserService getParser() {
        return new CommandLineDatesParserService();
    }

    private CommandLineDatesParserService() {
    }

    DateRange parse(String[] dateRange) {

        if (dateRange == null || dateRange.length != 2) {
            throw new IllegalArgumentException();
        }

        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");
        LocalDate date1 = LocalDate.parse(dateRange[0], formatter);
        LocalDate date2 = LocalDate.parse(dateRange[1], formatter);

        return new DateRange(date1, date2);

    }
}
