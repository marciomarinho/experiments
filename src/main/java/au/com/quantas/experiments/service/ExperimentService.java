package au.com.quantas.experiments.service;

import au.com.quantas.experiments.domain.Experiment;
import au.com.quantas.experiments.exception.ExperimentServiceException;

import java.time.format.DateTimeParseException;

public class ExperimentService {

    private String[] rawDateRange;

    private ExperimentService(String[] rawDateRange) {
        this.rawDateRange = rawDateRange;
    }

    public static ExperimentService getService(String[] rawDateRange) {
        return new ExperimentService(rawDateRange);
    }

    public long getElapsedDays() throws ExperimentServiceException {
        CommandLineDatesParserService parser = CommandLineDatesParserService.getParser();
        try {
            Experiment experiment = new Experiment(parser.parse(this.rawDateRange));
            return experiment.calculateElapsedDays();
        } catch (IllegalArgumentException | DateTimeParseException ex) {
            throw new ExperimentServiceException("Wrong arguments passed");
        }
    }
}
