package au.com.quantas.experiments.domain;

import java.time.LocalDate;

public class DateRange {

    private LocalDate startDate;
    private LocalDate endDate;

    public DateRange(LocalDate date1, LocalDate date2) {
        if (date1.isBefore(date2) || date1.isEqual(date2)) {
            this.startDate = date1;
            this.endDate = date2;
        } else {
            this.startDate = date2;
            this.endDate = date1;
        }
    }

    public LocalDate getStartDate() {
        return startDate;
    }

    public LocalDate getEndDate() {
        return endDate;
    }

}
