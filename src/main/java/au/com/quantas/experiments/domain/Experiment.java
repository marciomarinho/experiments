package au.com.quantas.experiments.domain;

import java.time.temporal.ChronoUnit;

public class Experiment {

    private DateRange range;

    public Experiment(DateRange range) {
        this.range = range;
    }

    public long calculateElapsedDays() {
        return ChronoUnit.DAYS.between(range.getStartDate(), range.getEndDate()) -1;
    }

}
