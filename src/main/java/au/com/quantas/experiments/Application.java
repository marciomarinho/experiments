package au.com.quantas.experiments;

import au.com.quantas.experiments.exception.ExperimentServiceException;
import au.com.quantas.experiments.service.ExperimentService;

public class Application {

    private long elapsedDays;
    private boolean usageCalled;

    public static void main(String[] args) {

        Application application = new Application();
        application.run(args);

    }

    public void run(String[] args) {

        ExperimentService experimentService = ExperimentService.getService(args);
        try {
            elapsedDays = experimentService.getElapsedDays();
            System.out.println( elapsedDays + " days");
        } catch (ExperimentServiceException e) {
            usage();
        }

    }

    void usage() {
        System.out.println("##########################################################");
        System.out.println("# Usage: java -jar experiments-1.0.jar startDate endDate #");
        System.out.println("##########################################################");
        System.out.println("");
        System.out.println("e.g  : java -jar experiments-1.0.jar 02/06/1983 22/06/1983");
        usageCalled = true;
    }

    long getElapsedDays() {
        return elapsedDays;
    }

    boolean isUsageCalled() {
        return usageCalled;
    }

}
