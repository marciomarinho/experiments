package au.com.quantas.experiments.exception;

public class ExperimentServiceException extends Exception {

    public ExperimentServiceException(String message) {
        super(message);
    }
}
