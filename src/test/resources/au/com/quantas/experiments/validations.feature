Feature: Experiments Application Validations

  Scenario Outline: Print usage when the scientist enters wrong parameter(s), and the program fails to parse
  the required parameters
    Given The experiment has <date1> and <date2>
    When The scientist runs the experiments application
    Then He should <expectation> the program usage

    Examples:
      | date1      | date2      | expectation |
      | abcdef     | 08/11/1972 | see         |
      | 08/11/1972 | abcdef     | see         |
      | 01/01/2000 | 03/01/2000 | not see     |