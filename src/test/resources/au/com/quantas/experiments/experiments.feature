Feature: Experiments

  Scenario Outline: Calculate elapsed days on experiment between two dates
    Given The experiment has <date1> and <date2>
    When The scientist runs the experiments application
    Then He should see <result> days

    Examples:
      | date1      | date2      | result |
      | 07/11/1972 | 08/11/1972 | 0      |
      | 01/01/2000 | 03/01/2000 | 1      |
      | 02/06/1983 | 22/06/1983 | 19     |
      | 04/07/1984 | 25/12/1984 | 173    |
      | 03/01/1989 | 03/08/1983 | 1979   |
