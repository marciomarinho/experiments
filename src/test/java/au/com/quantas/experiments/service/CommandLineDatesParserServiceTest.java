package au.com.quantas.experiments.service;

import au.com.quantas.experiments.domain.DateRange;
import org.junit.Test;
import org.mockito.internal.matchers.apachecommons.ReflectionEquals;

import java.time.LocalDate;
import java.time.format.DateTimeParseException;

import static org.junit.Assert.assertThat;

public class CommandLineDatesParserServiceTest {

    private DateRange expectedRange = new DateRange(LocalDate.of(2000,1,1), LocalDate.of(2000,01,03));
    private CommandLineDatesParserService parser = CommandLineDatesParserService.getParser();

    @Test
    public void testValidParameters() {
        String[] validParam = {"01/01/2000", "03/01/2000"};
        assertThat(expectedRange, new ReflectionEquals(parser.parse(validParam)));
    }

    @Test(expected=IllegalArgumentException.class)
    public void testInvalidParametersLength() {
        String[] invalidParam = {"03/01/2000"};
        assertThat(expectedRange, new ReflectionEquals(parser.parse(invalidParam)));
    }

    @Test(expected=DateTimeParseException.class)
    public void testInvalidParametersFormatNonDate() {
        String[] invalidParam = {"abcde", "03/01/2000"};
        assertThat(expectedRange, new ReflectionEquals(parser.parse(invalidParam)));
    }

    @Test(expected=DateTimeParseException.class)
    public void testInvalidParametersFormatWrongDateFormat() {
        String[] invalidParam = {"31/13/2012", "03/01/2000"};
        assertThat(expectedRange, new ReflectionEquals(parser.parse(invalidParam)));
    }

}
