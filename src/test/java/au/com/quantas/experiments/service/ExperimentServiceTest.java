package au.com.quantas.experiments.service;

import au.com.quantas.experiments.exception.ExperimentServiceException;
import org.junit.Test;
import org.mockito.Mockito;

import java.time.format.DateTimeParseException;

import static org.mockito.Mockito.when;

public class ExperimentServiceTest {

    @Test(expected = ExperimentServiceException.class)
    public void testWithEmptyParamsTranslateIllegalArgumentExceptionToExperimentServiceException() throws ExperimentServiceException {
        CommandLineDatesParserService service = Mockito.mock(CommandLineDatesParserService.class);
        when(service.parse(new String[]{})).thenThrow(new IllegalArgumentException("Wrong parameters"));
        ExperimentService experimentService = ExperimentService.getService(new String[]{});
        experimentService.getElapsedDays();
    }

    @Test(expected = ExperimentServiceException.class)
    public void testWithNullParamsTranslateIllegalArgumentExceptionToExperimentServiceException() throws ExperimentServiceException {
        CommandLineDatesParserService service = Mockito.mock(CommandLineDatesParserService.class);
        when(service.parse(new String[]{})).thenThrow(new IllegalArgumentException("Wrong parameters"));
        ExperimentService experimentService = ExperimentService.getService(null);
        experimentService.getElapsedDays();
    }

    @Test(expected = ExperimentServiceException.class)
    public void testWithEmptyParamsTranslateDateTimeParseExceptionToExperimentServiceException() throws ExperimentServiceException {
        CommandLineDatesParserService service = Mockito.mock(CommandLineDatesParserService.class);
        when(service.parse(new String[]{})).thenThrow(new DateTimeParseException("Wrong Date","123",0));
        ExperimentService experimentService = ExperimentService.getService(new String[]{});
        experimentService.getElapsedDays();
    }

}
