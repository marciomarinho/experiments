package au.com.quantas.experiments.domain;

import org.junit.Test;

import java.time.LocalDate;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;

public class ExperimentTest {

    @Test
    public void testExperimentElapsedDaysShouldReturn0() {
        Experiment experiment = new Experiment(new DateRange(LocalDate.of(1972, 11, 7), LocalDate.of(1972,11,8)));
        assertThat(experiment.calculateElapsedDays(), is(0L));
    }

    @Test
    public void testExperimentElapsedDaysShouldReturn1() {
        Experiment experiment = new Experiment(new DateRange(LocalDate.of(2000, 1, 1), LocalDate.of(2000,1,3)));
        assertThat(experiment.calculateElapsedDays(), is(1L));
    }

    @Test
    public void testExperimentElapsedDaysShouldReturn19() {
        Experiment experiment = new Experiment(new DateRange(LocalDate.of(1983, 6, 2), LocalDate.of(1983,6,22)));
        assertThat(experiment.calculateElapsedDays(), is(19L));
    }

    @Test
    public void testExperimentElapsedDaysShouldReturn173() {
        Experiment experiment = new Experiment(new DateRange(LocalDate.of(1984, 7, 4), LocalDate.of(1984,12,25)));
        assertThat(experiment.calculateElapsedDays(), is(173L));
    }

    @Test
    public void testExperimentElapsedDaysShouldReturn1979() {
        Experiment experiment = new Experiment(new DateRange(LocalDate.of(1989, 1, 3), LocalDate.of(1983,8,3)));
        assertThat(experiment.calculateElapsedDays(), is(1979L));
    }

}
