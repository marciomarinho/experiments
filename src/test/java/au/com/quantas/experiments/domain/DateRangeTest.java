package au.com.quantas.experiments.domain;

import org.junit.Test;

import java.time.LocalDate;

import static junit.framework.Assert.assertEquals;

public class DateRangeTest {

    @Test
    public void testDateRangeStartDateLessThanEndDate() {
        DateRange range = new DateRange(LocalDate.of(2000, 1, 1), LocalDate.of(2000,1,3));
        assertEquals(range.getStartDate(), LocalDate.of(2000, 1, 1));
    }

    @Test
    public void testDateRangeStartDateGraterThanEndDate() {
        DateRange range = new DateRange(LocalDate.of(2000,1,3), LocalDate.of(2000, 1, 1));
        assertEquals(range.getStartDate(), LocalDate.of(2000, 1, 1));
    }

    @Test
    public void testDateRangeStartDateEqualsToEndDate() {
        DateRange range = new DateRange(LocalDate.of(2000, 1, 1), LocalDate.of(2000, 1, 1));
        assertEquals(range.getStartDate(), LocalDate.of(2000, 1, 1));
    }

}
