package au.com.quantas.experiments;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

@RunWith(PowerMockRunner.class)
@PrepareForTest(Application.class)
public class ApplicationTest {

    private Application application;

    @Before
    public void setup() {
        application = new Application();
    }

    @Test
    public void testMainNewInstanceRun() throws Exception {
        Application applicationMock = Mockito.mock(Application.class);
        PowerMockito.whenNew(Application.class).withNoArguments().thenReturn(applicationMock);
        Application.main(new String[]{"01/01/1980", "01/02/1980"});
        PowerMockito.verifyNew(Application.class).withNoArguments();
        Mockito.verify(applicationMock).run(new String[]{"01/01/1980", "01/02/1980"});
    }

    @Test
    public void testApplicationUsageEnteringZeroParameters() {
        application.run(new String[]{});
        assertThat(application.isUsageCalled(), is(true));
    }

    @Test
    public void testApplicationUsageEnteringThreeParameters() {
        application.run(new String[]{"01/01/1980", "01/02/1980", "01/03/1980"});
        assertThat(application.isUsageCalled(), is(true));
    }

    @Test
    public void testApplicationUsageEnteringFirstParameterWithWrongType() {
        application.run(new String[]{"abcdef", "01/01/1980"});
        assertThat(application.isUsageCalled(), is(true));
    }

    @Test
    public void testApplicationUsageEnteringSecondParameterWithWrongType() {
        application.run(new String[]{"01/01/1980", "abcdef"});
        assertThat(application.isUsageCalled(), is(true));
    }

    @Test
    public void testApplicationUsageNotCalled() {
        application.run(new String[]{"01/01/1980", "01/03/1980"});
        assertThat(application.isUsageCalled(), is(false));
    }

}
