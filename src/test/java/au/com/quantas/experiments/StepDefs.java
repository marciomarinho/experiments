package au.com.quantas.experiments;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;

public class StepDefs {

    private String[] params;
    private Application application;

    @Given("^The experiment has (.+) and (.+)$")
    public void the_experiment_has_and(String date1, String date2) throws Throwable {
        params = new String[]{date1, date2};
    }

    @When("^The scientist runs the experiments application$")
    public void the_scientist_runs_the_experiments_application() throws Throwable {
        application = new Application();
        application.run(params);
    }

    @Then("^He should see (\\d+) days$")
    public void he_should_see_days(long arg1) throws Throwable {
        assertThat(application.getElapsedDays(), is(arg1));
    }

    @Then("^He should (.+) the program usage$")
    public void he_should_see_true_for_usage_class(String expectation) throws Throwable {
        boolean expectedBehaviour = expectation.equals("see") ? true : false;
        assertThat(application.isUsageCalled(), is(expectedBehaviour));
    }


}
