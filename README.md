# README #

### What is this repository for? ###

* Experiments Test Application
* 1.0

### Detailed info  ###

* This application was built using :
* Apache Maven 3.2.5 (12a6b3acb947671f09b81f49094c53f426d8cea1; 2014-12-15T04:29:23+11:00)
* Java version: 1.8.0_60, vendor: Oracle Corporation
* JUnit
* Mockito
* Powermock
* Cucumber

### Building from source ###

git clone / download source code

mvn clean package

### Running the application ###

* Download or build the executable jar from source code : experiments-1.0.jar
* java -jar experiments-1.0.jar 02/06/1983 22/06/1983

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact